const dialog = require("../index.js")
const mq = require('mqtt')
const topic1 = "mqtt-dialog-ffhb";
const topic2 = "mqtt-dialog-ffhc"


let mqtt

beforeEach(done => {
  mqtt = mq.connect("mqtt://test.mosquitto.org")
  mqtt.on('connect', done)
})

afterEach(done => {
  mqtt.end(done)
})
/**
 * Create 2 conversation partners, p1 and p2. P1 sends a message "hello" to p2 and expects it to reply
 * "world". 
 */
describe('Mqtt-Dialog', () => {
  it('should send and reply a message', done => {
    const p1 = dialog(mqtt, topic1, (id, incoming) => { }, { id: "p1" })
    const p2 = dialog(mqtt, topic1, (id, incoming) => {
      expect(incoming).toEqual("hello")
      p2.reply(id, "world")
    }, { id: "p2" })
    p1.post("hello").then(ans => {
      expect(ans).toEqual("world")
      p1.detach()
      p2.detach()
      done()
    })
  })
  it('should fail after timeout', done => {
    const p3 = dialog(mqtt, topic2, (id, incoming) => {
    }, { timeout: 100, id: "p3" })
    const p4 = dialog(mqtt, topic2, (id, incoming) => {
      expect(incoming).toEqual("hello")
      // don't reply
    }, { id: "p4" })
    p3.post("hello").then(ans => {
      done.fail("shouldn't get a reply here")
    }).catch(err => {
      p3.detach()
      p4.detach()
      done()
    })
  })
  it('can handle multiple messages in the queue', done => {
    const jobs = []
    const times = []
    const msgs = new Map()
    const p5 = dialog(mqtt, topic1, (id, incoming) => {
      p5.reply(id, "a" + incoming)
    }, { id: "p5" })
    const p6 = dialog(mqtt, topic1, (id, incoming) => {
      fail("should't get a message here")
    }, { id: "p6" })
    const p7 = dialog(mqtt, topic2, (id, incoming) => {
      p7.reply(id, "a" + incoming)
    }, { id: "p7" })
    const p8 = dialog(mqtt, topic2, (id, incoming) => {
      fail("should't get a message here")
    }, { id: "p8" })
 
    for (let i = 0; i < 10; i++) {
      msgs.set("a" + i.toString(), new Date().getTime())
      jobs.push(p6.post(i.toString()))
      msgs.set("a" + (i+10).toString(), new Date().getTime())
      jobs.push(p8.post((i+10).toString()))
    }
    Promise.all(jobs).then(replies => {
      replies.forEach(ans => {
        if (!msgs.has(ans)) {
          fail("got bad message")
        } else {
          let end = new Date().getTime()
          times.push(end - msgs.get(ans))
          msgs.delete(ans)
        }
        if (msgs.size == 0) {
          console.log("avg round trip time: "+times.reduce((a,b )=>a+b)/times.length)
          p5.detach()
          p6.detach()
          p7.detach()
          p8.detach()
          done()
        }
      })
    })
  })
  it('can handle JSON messages',done=>{
    const testMsg={
      "string": "json",
      "number": 7.2,
      "boolean": true,
      "array": [1,2,4],
      object: {
        "a":"b",
        "c": 42
      }
    }
    const p9=dialog(mqtt,topic1,(id,msg)=>{
      expect(msg).toEqual(testMsg)
      done();
    })
    const p10=dialog(mqtt,topic1,()=>{})
    p10.post(testMsg)
  })
})