const dialog=require("./index")
const broker="mqtt://test.mosquitto.org"
const topic="diytestdialog"
const mqtt=require("mqtt").connect(broker)

mqtt.on('connect',()=>{
  console.log("connected")
  const number1=dialog(mqtt,topic,(id,msg)=>{
    console.log(`number1 received ${msg} with id ${id}`)
  })
  
  const number2=dialog(mqtt,topic,(id,msg)=>{
    console.log(`number2 received "${msg}"`)
    number2.reply(id,"world")
  })
  
  number1.post("hello").then(reply=>{
    console.log(`number 1 received reply "${reply}"`)
  }).catch(err=>{
    console.log(err)
  })
  number1.post("unanswered")
})


