/**
 * MQTT based dialog
 * Copyright (c) 2018 by rgwch
 */
const uuid = require('uuid').v4

function mqtt_dialog(mqtt, topic, receiver, options) {
  const cfg = Object.assign({}, {
    id: uuid(),
    timeout: 500 // milliseconds
  }, options)

  const pending = new Map()
  mqtt.subscribe(topic)

  /**
   * Publish a message and wait for a reply (exported)
   * @param {any} msg
   * @returns Promise resolving to the answer or rejecting with the error reason (which can be "timeout") 
   */
  function post(msg) {
    return new Promise((resolve, reject) => {
      send(msg, (err, ans) => {
        if (err) {
          reject(err)
        } else {
          resolve(ans)
        }
      })
    })
  }

  /**
   * Reply to a message, previously *post*ed by another partner. (exported)
   * @param {string} id the Message ID of the received message 
   * @param {any} message the reply 
   */
  function reply(id, message) {
    return new Promise((resolve, reject) => {
      const msg = {
        key: id,
        message: message,
        sender: cfg.id
      }
      mqtt.publish(topic + "/" + id, JSON.stringify(msg), { qos: 1 }, err => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  }


  /*
   * Send a message (not exported).
   * A timeout is associated with the message and executed, if there's no reply
   * @param {any} message 
   * @param {(err,reply)=>{}} callback 
   */
  function send(message, callback) {
    const msgid = uuid()
    const msg = {
      key: msgid,
      message: message,
      sender: cfg.id
    }
    mqtt.subscribe(topic + "/" + msgid)
    mqtt.publish(topic, JSON.stringify(msg), { qos: 1 }, err => {
      if (err) {
        callback(err, null)
      }
    })

    pending.set(msg.key, Object.assign(msg, {
      callback: callback,
      time: new Date(),
      timeout: setTimeout(expired, cfg.timeout, msg.key),
    }))

  }

  /*
  * receice messages and replies. If it's a reply, the timeout is cleared and the reply
  * is transmitted to the original sender. If it's not a reply, it's treated as a new message.
  */
  mqtt.on("message", (msgtopic, message) => {
    try {
      const js = JSON.parse(message)
      if (js.sender !== cfg.id) {
        let msg = pending.get(js.key)
        if (msg) {
          clearTimeout(msg.timeout)
          pending.delete(js.key)
          mqtt.unsubscribe(topic + "/" + js.key)
          msg.callback(null, js.message)
        } else {
          if(msgtopic===topic){
            receiver(js.key, js.message)
          }
        }
      }
    } catch (err) {
      console.log("error " + err + " while receiving " + message.toString())
    }
  })

  /*
  called if a message expires
  */
  function expired(key) {
    const msg = pending.get(key)
    pending.delete(key)
    mqtt.unsubscribe(topic + "/" + key)
    msg.callback("message expired: " + msg.message, null)
  }

  function detach(){
    mqtt.unsubscribe(topic+"/#")
    mqtt.unsubscribe(topic)
    pending.clear()
  }

  return {
    post,
    reply,
    detach
  }
}

module.exports = mqtt_dialog