## About

Implement MQTT-based dialog.

While it might seem a bit odd to chose MQTT for dialogs, it can be useful: An MQTT Client does not impose much load on the machine or on the network and is easily used on small machines like the Raspberry Pi zero. So, this library allows to send a message and wait for a response (or get an error if no such response is received.)

## Install

npm install --save mqtt-dialog

## Example

For the sake of simplicity, both members of the conversation are created in the same file here. This is different in real applications, of course.

~~~~~~~~javascript
const dialog=require("mqtt-dialog")
const broker="mqtt://test.mosquitto.org"
const topic="mqttdialog/example"
const mqtt=require("mqtt").connect(broker)

mqtt.on('connect',()=>{
  console.log("connected")

  // create first member
  const number1=dialog(mqtt,topic,(id,msg)=>{
    console.log(`number1 received ${msg} with id ${id}`)
  })

  // second member
  const number2=dialog(mqtt,topic,(id,msg)=>{
    console.log(`number2 received "${msg}"`)
    number2.reply(id,"world")
  })
  
  // first member publishes a message and waits for a reply 
  number1.post("hello").then(reply=>{
    console.log(`number 1 received reply "${reply}"`)
  }).catch(err=>{
    console.log(err)
  })
})
~~~~~~~~

## API

### Constructor

    dialog(mqtt,topic,incoming,options?)

* mqtt is a fully configured and connected  [MQTT](https://www.npmjs.com/package/mqtt) client.

* topic is the base topic to use for the conversation. Mqtt-dialog will send messages on that topic and replies on a subtopic.

* incoming is a function which is called on incoming messages (wich are not replies). The function is called with the parameters (id,body), where id is a unique string and body is the message body. To reply, the id must be given.

* options can have the following attributes:

    * timeout: If no reply is received after that time, the Promise returned by *post* ist rejected. Defaults to 500ms

    * id: An identifier for this instance. Defaults to a generated uuid.

### post

  dialog.post(message) : Promise

Publish 'message' on the topic given with the constructor. Returns a Promise which is resolved with the contents of a reply to that message, or rejected with an error message.

### reply

  dialog.reply(id,text) : Promise

Reply to an incoming message as received by the "incoming" function of the constructor.

* id must be the id of the incoming message

* text can be any object or string

### detach

    dialog.detach()

Unsubscribe topics, clear queue and stop listening.

## Note

All messages are sent with a QOS-Setting of 1 (i.e. delivery once or more guaranteed). This said, the only possible reasons for a failure would be a failure of the broker, or the network, or a failure of the conversation partner. So, one possible use-case for mqtt-dialog could be an "alive"-test without the necessity to send regularly pings over the network:

~~~~~~~~javascript
echo.post("ping").then(ans=>{
      console.log(`${ans} is alive!`)
})
~~~~~~~~

Important: There is no "privacy" there, and there's no guarantee of the identity of the responder: Any client can subscribe to the topic and will receive all messages (but not the replies). If more than one client replies, only the first reply ist propagated to the sender of the original message. To create multiple conversation channels, use multiple topics.
